from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.db.models import Count


@login_required
def account_list(request):
    user = request.user
    account_list = Account.objects.filter(owner=user).annotate(
        num_receipts=Count("receipts")
    )
    context = {"account_list": account_list}
    return render(request, "receipts/accounts.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "accounts/create.html", context)


@login_required
def category_list(request):
    user = request.user
    expense_list = ExpenseCategory.objects.filter(owner=user).annotate(
        num_receipts=Count("receipts")
    )
    context = {"expense_list": expense_list}
    return render(request, "categories/list.html", context)


@login_required
def create_expense_catagory(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "categories/create.html", context)


@login_required
def receipt_list(request):
    user = request.user
    receipts = Receipt.objects.filter(purchaser=user)
    context = {"receipt_list": receipts}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {"form": form}

    return render(request, "receipts/create.html", context)
